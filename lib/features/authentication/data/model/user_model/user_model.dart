class UserModel {
  UserModel({ required this.uid, this.id, this.name, this.imgUrl});

  int? id;
  String uid;
  String? name;
  String? imgUrl;

}
