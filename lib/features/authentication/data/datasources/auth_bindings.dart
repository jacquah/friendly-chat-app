import 'package:flutter_demo/features/authentication/data/datasources/auth_controller.dart';
import 'package:get/get.dart';
class AuthBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AuthController>(() => AuthController());
  }

}