import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo/core/presentation/widgets/exceptionIndicators/exception_indicator.dart';
import 'package:flutter_demo/features/authentication/data/model/user_model/user_model.dart';
import 'package:get/get.dart';

class AuthController extends GetxController {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final ExceptionIndicator _exceptionIndicator = ExceptionIndicator();
  late Rx<User?> _firebaseUser;

  @override
  onInit() {
    _firebaseUser = Rx<User?>(_firebaseAuth.currentUser);
    _firebaseUser.bindStream(_firebaseAuth.authStateChanges());
    super.onInit();
  }

  //creating user object based on firebase User
  UserModel? _userModelFromFirebaseUser(User user) {
    return user != null ? UserModel(uid: user.uid) : null;
  }

  String? get user => _firebaseUser.value?.email;
  // listening to authentication changes
  /*Stream<UserModel?> get user {
    return _firebaseAuth
        .authStateChanges()
        .map((User? user) => _userModelFromFirebaseUser(user!));
  }*/

  Future signUp({required String email, required String password}) async {
    try {
      UserCredential userCredential =
          await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      User? user = userCredential.user;
     _exceptionIndicator.showSuccess(title: 'Log In',
         message: 'Account created successfully');
      return _userModelFromFirebaseUser(user!);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
         _exceptionIndicator.showInfo(title: 'Sign Up',
             message: 'The password provide is too weak');
        debugPrint('The password provided is too weak');
      } else if (e.code == 'email-already-in-use') {
        _exceptionIndicator.showError(title: 'Sign Up',
            message: 'Account already available for this email');
      }
    } catch (e) {
      debugPrint('error');
      debugPrint(e.toString());
    }
  }

  Future signIn({required String email, required String password}) async {
    try {
      UserCredential userCredential =
          await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      User? user = userCredential.user;
      _exceptionIndicator.showSuccess(title: 'Login',
          message: 'Account logged In successful' );
      return _userModelFromFirebaseUser(user!);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
       _exceptionIndicator.showError(title: 'Login',
           message: 'No user found for this email');
        debugPrint('No user found for that email');
      } else if (e.code == 'wrong-password') {
        _exceptionIndicator.showError(title: 'Login',
            message: 'Incorrect password');
        debugPrint('Wrong password provide for that user');
      }
    }
  }

  void signOut() async {
   try{
     await _firebaseAuth.signOut();
   }catch (e){
     debugPrint(e.toString());

   }
  }
}
