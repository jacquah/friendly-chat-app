import 'package:flutter/cupertino.dart';
import 'package:flutter_demo/features/authentication/data/datasources/auth_controller.dart';
import 'package:get/get.dart';

class SignUpController extends GetxController{
  // Reactive variables
  RxBool isObscure = true.obs;
  RxString email = ''.obs;
  RxString password = ''.obs;

  final formKey = GlobalKey<FormState>();

  final AuthController _authController = Get.find<AuthController>();

  void signUp(){
    final isValid = formKey.currentState!.validate();
    if(isValid) {
      _authController.signUp(email: email.value, password: password.value);
    }
  }

  void onEmailInputChange(String value){
    email(value);
  }

  void onPasswordInputChange(String value){
    password(value);
  }

  String? validateEmail(String? value){
    String? errorMessage ;
    if(value!.isEmpty || !value.contains('@')){
      return errorMessage = 'Please enter a correct email';
    }else {
      return errorMessage;
    }
  }


  String? passwordValidator(String value) {
    if (value.isEmpty || value.length < 6) {
      return 'Password must be at least 6 characters long.';
    }
    return null;
  }
}