import 'package:flutter/material.dart';
import 'package:flutter_demo/core/presentation/widgets/app_text_input.dart';
import 'package:flutter_demo/features/authentication/presentation/signup/getx/signup_controller.dart';
import 'package:get/get.dart';
class SignUpScreen extends GetView<SignUpController> {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign Up'),
      ),
      body: Form(
        key: controller.formKey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              AppTextInput(
                onChanged: (value) =>
                    controller.onEmailInputChange(value!),
                validator: (value) =>
                controller.validateEmail(value),
                hintText: 'Email',
              ),
              Obx( () {
                  return AppTextInput(
                    onChanged: (value) =>
                        controller.onPasswordInputChange(value!),
                    validator: (value) =>
                    controller.passwordValidator(value!),
                    suffixIcon: IconButton(
                      icon: controller.isObscure.value ?
                      const Icon(Icons.remove_red_eye)
                          : const Icon(Icons.visibility_off),
                      onPressed: () {
                        controller.isObscure.value = !controller.isObscure.value;
                      },
                    ),
                    obscureText: controller.isObscure.value,
                    hintText: 'Password',
                  );
                }
              ),
              ElevatedButton(
                child: const Text('Register'),
                onPressed: () => {
                  controller.signUp(),
                  debugPrint(controller.email.value),
                  debugPrint(controller.password.value),
                },
              )
            ],
          ),
        ) ,
      ),
    );
  }
}
