import 'package:flutter/material.dart';
import 'package:flutter_demo/core/presentation/widgets/app_text_input.dart';
import 'package:flutter_demo/features/authentication/presentation/login/getx/login_controller.dart';
import 'package:get/get.dart';

class LoginScreen extends GetView<LoginController> {
   LoginScreen({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormFieldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Log In'),
        actions: [
          TextButton(
            onPressed: ()=> {
              controller.navigateToSignUpPage()
            },
            child: const Text('Sign Up',
            style: TextStyle(
              color: Colors.white,
            ),),
          )
        ],
      ),
      body: Form(
        key: controller.formKey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
             AppTextInput(
              onChanged: (value) =>
               controller.onEmailInputChange(value!),
              validator: (value) =>
                controller.validateEmail(value),
              hintText: 'Email',
             ),
              Obx(() {
                  return AppTextInput(
                    onChanged: (value) =>
                        controller.onPasswordInputChange(value!),
                    validator: (value) =>
                      controller.passwordValidator(value!),
                    suffixIcon: IconButton(
                      icon: controller.isObscure.value ?
                        const Icon(Icons.remove_red_eye)
                      : const Icon(Icons.visibility_off),
                      onPressed: () {
                        controller.isObscure.value = 
                        !controller.isObscure.value;
                      },
                    ),
                    obscureText: controller.isObscure.value,
                    hintText: 'Password',
                  );
                }
              ),
              ElevatedButton(
                child: const Text('Log In'),
                onPressed: () => {
                    controller.login(),
                    print(controller.password.value),
                    print(controller.email.value)

                },
              )
            ],
          ),
        ) ,
      ),
    );
  }
}
