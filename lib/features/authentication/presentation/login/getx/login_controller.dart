import 'package:flutter/cupertino.dart';
import 'package:flutter_demo/core/presentation/routes/app_routes.dart';
import 'package:flutter_demo/features/authentication/data/datasources/auth_controller.dart';
import 'package:get/get.dart';

class LoginController extends GetxController{

  // authController
  final AuthController _authController = Get.find<AuthController>();

  // global form key
  final formKey = GlobalKey<FormState>();

  // Reactive variables
  RxBool isObscure = true.obs;
  RxString email = ''.obs;
  RxString password = ''.obs;


  void login(){
    final isValid = formKey.currentState!.validate();
    if(isValid) {
      _authController.signIn(email: email.value, password: password.value);
    }
  }
  void navigateToSignUpPage(){
    Get.toNamed(AppRoutes.singUp);
  }

  void onEmailInputChange(String value){
    email(value);
  }

  void onPasswordInputChange(String value){
    password(value);
  }

  String? validateEmail(String? value){
    String? errorMessage ;
    if(value!.isEmpty || !value.contains('@')){
      return errorMessage = 'Please enter a valid email address.';
    }else {
      return errorMessage;
    }
  }

  String? passwordValidator(String value) {
    if (value.isEmpty || value.length < 6) {
      return 'Password must be at least 6 characters long.';
    }
    return null;
  }
}