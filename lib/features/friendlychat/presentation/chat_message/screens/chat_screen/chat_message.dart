import 'package:flutter/material.dart';
import 'package:flutter_demo/core/presentation/widgets/custom_bubble_widget.dart';

class ChatMessage extends StatelessWidget {
  const ChatMessage(
      {required this.text, Key? key, required this.animationController})
      : super(key: key);
  final String text;
  final AnimationController animationController;

  final bool isSender = false;
  @override
  Widget build(BuildContext context) {
    String _name = '';
    return SizeTransition(
      sizeFactor:
          CurvedAnimation(parent: animationController, curve: Curves.easeOut),
      axisAlignment: 0.0,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: isSender ? CrossAxisAlignment.end
                                           : CrossAxisAlignment.start,
              children: [
                Text(
                  _name,
                  style: const TextStyle(fontSize: 12),
                ),
               CustomBubble(
                 text: text,
                 sent: true,
                 seen: true,
                 isSender: isSender,

               ),

              ],
            ),
          )
        ],
      ),
    );
  }
}
