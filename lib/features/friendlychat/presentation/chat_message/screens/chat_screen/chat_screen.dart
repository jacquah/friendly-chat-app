import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo/core/presentation/widgets/text_composer_widget.dart';
import 'package:flutter_demo/core/presentation/themes/custom_colors.dart';
import 'package:flutter_demo/features/friendlychat/presentation/chat_message/getx/chat_message_controller.dart';
import 'package:get/get.dart';

import 'chat_message.dart';

class ChatScreen extends GetWidget<ChatMessageController> {
  ChatScreen({Key? key}) : super(key: key);

  late final TextEditingController _textEditingController =
      TextEditingController();
  late final FocusNode _myFocusNode = FocusNode();

  late bool isShowEmoji = false;

  _onEmojiSelected(Emoji emoji) {
    _textEditingController
      ..text += emoji.emoji
      ..selection = TextSelection.fromPosition(
          TextPosition(offset: _textEditingController.text.length));
  }

  _onBackspacePressed() {
    _textEditingController
      ..text = _textEditingController.text.characters.skipLast(1).toString()
      ..selection = TextSelection.fromPosition(
          TextPosition(offset: _textEditingController.text.length));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: primaryColor,
      appBar: AppBar(
        centerTitle: true,
        title: const Text('John'),
        elevation: 0.0,
        actions:  [
          IconButton(onPressed: () => controller.signOut(),
              icon: const Icon(Icons.logout))
        ],
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
          controller.showEmoji.value = true;
        },
        child: Container(
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
              )),
          child: Column(
            children: [
              Obx(() => Flexible(
                    child: ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(30.0),
                        topRight: Radius.circular(30.0),
                      ),
                      child: ListView.builder(
                        itemBuilder: (_, int index) {
                          return controller.messages[index];
                        },
                        padding: const EdgeInsets.all(8.0),
                        itemCount: controller.messages.length,
                        reverse: true,
                      ),
                    ),
                  )),
              Obx(() {
                return buildTextComposer(
                  hintText: 'Type a message',
                  controller: _textEditingController,
                  onChanged: (text) {
                    controller.isComposing.value = text.isNotEmpty;
                  },
                  onSubmitted:
                      controller.isComposing.value ? _handleSubmitted : null,
                  onSendIconPressed: () => (controller.isComposing.value)
                      ? _handleSubmitted(_textEditingController.text)
                      : null,
                  focusNode: _myFocusNode,
                  readOnly: controller.showEmoji.value == true ? false : true,
                  onEmojiIconPressed: () {
                    controller.showEmoji.value = !controller.showEmoji.value;
                  },
                  offstage: controller.showEmoji.value,
                  onBackspacePressed: _onBackspacePressed,
                  onEmojiSelected: (Category category, Emoji emoji) {
                    _onEmojiSelected(emoji);
                  },
                );
              }),
            ],
          ),
        ),
      ),
    );
  }

  _handleSubmitted(String text) {
    _textEditingController.clear();
    controller.isComposing.value = false;
    var _message = ChatMessage(
        text: text, animationController: controller.animationController);

    controller.messages.insert(0, _message);
    _myFocusNode.requestFocus();
    _message.animationController.forward();
  }

  void dispose() {
    for (var message in controller.messages) {
      message.animationController.dispose();
    }
  }
}
