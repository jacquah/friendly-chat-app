import 'package:flutter/animation.dart';
import 'package:flutter_demo/features/authentication/data/datasources/auth_controller.dart';
import 'package:flutter_demo/features/friendlychat/presentation/chat_message/screens/chat_screen/chat_message.dart';
import 'package:get/get.dart';

class ChatMessageController extends GetxController
    with GetSingleTickerProviderStateMixin {
  List<ChatMessage> messages = <ChatMessage>[].obs;

  RxBool isComposing = false.obs;
  RxBool showEmoji = true.obs;

  late AnimationController animationController;

  final AuthController _authController = Get.find<AuthController>();

  @override
  void onInit() {
    super.onInit();
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );
  }

  @override
  void dispose() {
    super.dispose();
    animationController.dispose();
  }

  void signOut(){
    _authController.signOut();
  }
}
