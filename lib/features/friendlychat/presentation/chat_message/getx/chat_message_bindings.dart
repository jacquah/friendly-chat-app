import 'package:flutter_demo/features/friendlychat/presentation/chat_message/getx/chat_message_controller.dart';
import 'package:get/get.dart';

class ChatMessageBindings extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => ChatMessageController());
  }

}