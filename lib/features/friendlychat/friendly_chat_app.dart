import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo/core/presentation/routes/app_routes.dart';
import 'package:flutter_demo/core/presentation/routes/pages.dart';
import 'package:flutter_demo/features/authentication/data/datasources/auth_bindings.dart';
import 'package:flutter_demo/features/authentication/presentation/login/getx/login_bindings.dart';
import 'package:get/get.dart';

import '../../core/presentation/themes/app_theme.dart';
class FriendlyChatApp extends StatelessWidget {
  const FriendlyChatApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'FriendlyChat',
      theme: defaultTargetPlatform == TargetPlatform.iOS
          ? AppTheme().kIOSThemeL
          : AppTheme().kDefaultTheme,
      initialRoute: AppRoutes.root ,
      getPages: Pages.pages,
      initialBinding: AuthBindings(),
    );
  }
}