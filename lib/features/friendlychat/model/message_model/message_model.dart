import 'package:flutter_demo/features/authentication/data/model/user_model/user_model.dart';

class Message {
  Message(
      {required this.text,
      required this.time,
      required this.sender,
      required this.unread});

  UserModel sender;
  String text;
  String time;
  bool unread;
}
