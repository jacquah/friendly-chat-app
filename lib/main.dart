import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo/features/friendlychat/friendly_chat_app.dart';
import 'package:get/get.dart';
import 'core/presentation/themes/custom_colors.dart';
import 'features/friendlychat/presentation/chat_message/getx/chat_message_controller.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
     debugShowCheckedModeBanner: false,
      title: 'Friendly Chat App',
      theme: ThemeData(
         backgroundColor: Colors.white,
        appBarTheme:  AppBarTheme(
          backgroundColor: primaryColor,
        )
      ),
      home:  const FriendlyChatApp(),
    );
  }
}
