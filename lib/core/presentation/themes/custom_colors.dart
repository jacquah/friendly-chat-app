import 'package:flutter/material.dart';

Color primaryColor = Colors.purple;
Color secondaryColor1 = const Color(0xffdad8d8);
Color secondaryColor2 = const Color(0xffc785c7);
Color textPrimaryColor1 = const Color(0xffe0e0e0);
Color textPrimaryColor2 = const Color(0xff3a3939);

