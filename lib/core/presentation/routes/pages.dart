import 'package:flutter_demo/core/presentation/routes/app_routes.dart';
import 'package:flutter_demo/core/root/root.dart';
import 'package:flutter_demo/features/authentication/presentation/login/getx/login_bindings.dart';
import 'package:flutter_demo/features/authentication/presentation/login/screens/login_screen.dart';
import 'package:flutter_demo/features/authentication/presentation/signup/getx/singup_bindings.dart';
import 'package:flutter_demo/features/authentication/presentation/signup/screens/sign_up_screen.dart';
import 'package:flutter_demo/features/friendlychat/presentation/chat_message/getx/chat_message_bindings.dart';
import 'package:flutter_demo/features/friendlychat/presentation/chat_message/screens/chat_screen/chat_screen.dart';
import 'package:get/get.dart';

class Pages {
  static List<GetPage<AppRoutes>> pages = [
    GetPage<AppRoutes>(
      name: AppRoutes.root,
      page: () => Root(),
      bindings: [
        LoginBindings(),
        ChatMessageBindings(),
      ]
    ),
    GetPage<AppRoutes>(
      name: AppRoutes.login,
      page: () => LoginScreen(),
      binding: LoginBindings(),
    ),
    GetPage<AppRoutes>(
      name: AppRoutes.singUp,
      page: () => const SignUpScreen(),
      binding: SignUpBindings(),
    ),
    GetPage<AppRoutes>(
      name: AppRoutes.chatScreen,
      page: () => ChatScreen(),
      binding: ChatMessageBindings(),
    ),
  ];
}
