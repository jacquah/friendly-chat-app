import 'package:flutter/material.dart';

class AppTextInput extends StatelessWidget {
  AppTextInput({
    required this.onChanged,
    required this.validator,
    required this.hintText,
    this.obscureText,
    this.suffixIcon,
    Key? key,
  }) : super(key: key);

  final String? Function(String?) validator;
  final Function(String?) onChanged;
  Widget? suffixIcon;
  bool? obscureText = false;
  final String? hintText;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChanged,
      validator: validator,
      obscureText: obscureText ?? false,
      decoration: InputDecoration(
        hintText: hintText,
        suffixIcon: suffixIcon,

      ),
    );
  }
}
