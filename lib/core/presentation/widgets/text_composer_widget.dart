import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo/core/presentation/themes/custom_colors.dart';
import 'package:flutter_demo/features/friendlychat/presentation/chat_message/getx/chat_message_controller.dart';
import 'package:get/get.dart';
import 'package:flutter/services.dart';

import 'emoji_widget.dart';


Widget buildTextComposer(
    {required String hintText,
    required TextEditingController controller,
    required Function(String) onChanged,
    required Function(String)? onSubmitted,
    required VoidCallback onSendIconPressed,
    required FocusNode focusNode,
    required VoidCallback? onEmojiIconPressed,
    required bool offstage,
    required bool readOnly,
    required VoidCallback? onBackspacePressed,
    required void Function(Category, Emoji) onEmojiSelected}) {

  ChatMessageController chatMessageController = Get.find();
  return Column(
    children: [
      Row(
        children: [
          IconButton(
            icon: Icon(Icons.image, color: primaryColor),
            onPressed: (){},
          ),
          Flexible(
            child: Card(
              color: Colors.black54,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
              child: Row(
                children: [
                  IconButton(
                    icon: Icon(Icons.insert_emoticon, color: textPrimaryColor2),
                    onPressed: onEmojiIconPressed,
                  ),
                  Expanded(
                    child: TextField(
                      cursorColor: textPrimaryColor1,
                      style: TextStyle(color: textPrimaryColor1),
                      decoration: InputDecoration(
                          hintText: hintText,
                          hintStyle: TextStyle(
                            color: textPrimaryColor2,
                          ),
                          border: InputBorder.none),
                      controller: controller,
                      showCursor: true,
                      readOnly: readOnly,
                      onChanged: onChanged,
                      onSubmitted: onSubmitted,
                      focusNode: focusNode,
                      textCapitalization: TextCapitalization.sentences,
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      inputFormatters: [
                        NoLeadingSpaceFormatter(),
                      ],
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.attach_file_outlined, color: textPrimaryColor1),
                    onPressed: (){},
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 4.0),
            child: Obx( () {
                return chatMessageController.isComposing.value ? CircleAvatar(
                  backgroundColor: primaryColor,
                  child: IconButton(
                      icon: const Icon(
                        Icons.send ,
                        size: 20,
                        color: Colors.white,
                      ),
                      onPressed: onSendIconPressed),
                )
                : CircleAvatar(
                  backgroundColor: primaryColor,
                  child: IconButton(
                      icon: const Icon(
                        Icons.mic,
                        color: Colors.white,
                      ),
                      onPressed: onSendIconPressed),
                ) ;
              }
            ),
          ),
        ],
      ),
      EmojiWidget(
        offstage: offstage,
        onBackspacePressed: onBackspacePressed,
        onEmojiSelected: onEmojiSelected,
      )
    ],
  );
}

class NoLeadingSpaceFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    if (newValue.text.startsWith(' ')) {
      final String trimmedText = newValue.text.trimLeft();

      return TextEditingValue(
        text: trimmedText,
        selection: TextSelection(
          baseOffset: trimmedText.length,
          extentOffset: trimmedText.length,
        ),
      );
    }

    return newValue;
  }
}
