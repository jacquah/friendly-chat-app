import 'dart:io';

import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmojiWidget extends StatelessWidget {
   EmojiWidget({required this.offstage,
   required this.onBackspacePressed,
   required this.onEmojiSelected,
     Key? key}) : super(key: key);

  bool offstage = false;
 void Function(Category, Emoji) onEmojiSelected;
  VoidCallback? onBackspacePressed;
  @override
  Widget build(BuildContext context) {
    return Offstage(
      offstage: offstage,
      child: SizedBox(
        height: 250,
        child: EmojiPicker(
            onEmojiSelected: onEmojiSelected,
            onBackspacePressed: onBackspacePressed,
            config: const Config(
                columns: 7,
               // emojiSizeMax: 32 * (Platform.isIOS ? 1.30 : 1.0),
                emojiSizeMax: 32 *  1.0,
                verticalSpacing: 0,
                horizontalSpacing: 0,
                initCategory: Category.RECENT,
                bgColor: Color(0xFFF2F2F2),
                indicatorColor: Colors.blue,
                iconColor: Colors.grey,
                iconColorSelected: Colors.blue,
                progressIndicatorColor: Colors.blue,
                backspaceColor: Colors.blue,
                skinToneDialogBgColor: Colors.white,
                skinToneIndicatorColor: Colors.grey,
                enableSkinTones: true,
                showRecentsTab: true,
                recentsLimit: 28,
                noRecentsText: 'No Recents',
                noRecentsStyle: TextStyle(
                    fontSize: 20, color: Colors.black26),
                tabIndicatorAnimDuration: kTabScrollDuration,
                categoryIcons: CategoryIcons(),
                buttonMode: ButtonMode.MATERIAL)),
      ),
    );
  }
}

