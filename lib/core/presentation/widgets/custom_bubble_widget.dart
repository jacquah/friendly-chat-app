import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo/core/presentation/themes/custom_colors.dart';

class CustomBubble extends StatelessWidget {
  CustomBubble(
      {required this.text,
      required this.seen,
      required this.sent,
      required this.isSender,
      Key? key})
      : super(key: key);

  String text;
  bool sent = false;
  bool seen = false;
  bool delivered = true;
  bool isSender = true;

  @override
  Widget build(BuildContext context) {
    Icon? tickedIcon;
    if (delivered) {
      tickedIcon = const Icon(
        Icons.done,
        size: 18,
        color: Color(0xFF97AD8E),
      );
    }

    if (sent) {
      tickedIcon = const Icon(
        Icons.done_all,
        size: 18,
        color: Color(0xff97AD8E),
      );
    }

    if (seen) {
      tickedIcon = const Icon(
        Icons.done_all,
        size: 18,
        color: Color(0xff92DEDA),
      );
    }
    return Container(
      child: Column(
        crossAxisAlignment:
            isSender ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
            decoration: BoxDecoration(
              color: isSender ? secondaryColor2 : secondaryColor1,
              borderRadius: isSender ? const BorderRadius.only(
                topRight: Radius.circular(19.0),
                bottomLeft: Radius.circular(19.0),
                topLeft: Radius.circular(19.0),
              ) :
              const BorderRadius.only(
                topRight: Radius.circular(19.0),
                bottomRight: Radius.circular(19.0),
                topLeft: Radius.circular(19.0),
              ),
            ),
            margin: isSender
                ? const EdgeInsets.only(top: 0.0, left: 40.0)
                : const EdgeInsets.only(top: 0.0, right: 40.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  clipBehavior: Clip.none,
                  children: [
                    Text(
                      text.trimRight(),
                      style: TextStyle(
                        color: isSender ? Colors.white : Colors.black,
                      ),
                    ),
                    tickedIcon != null
                        ? Positioned(
                            right: -14,
                            bottom: -10,
                            child: isSender ? tickedIcon : const SizedBox(),
                          )
                        : const SizedBox(
                            width: 1,
                          )
                  ],
                ),
              ],
            ),
          ),
          Text(
            TimeOfDay.now().format(context),
            style: const TextStyle(
              color: Colors.blueGrey,
              fontSize: 10,
            ),
            textAlign: TextAlign.end,
          ),
        ],
      ),
    );
  }
}
