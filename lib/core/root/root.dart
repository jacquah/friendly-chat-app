import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_demo/features/authentication/data/datasources/auth_controller.dart';
import 'package:flutter_demo/features/authentication/presentation/login/screens/login_screen.dart';
import 'package:flutter_demo/features/friendlychat/presentation/chat_message/screens/chat_screen/chat_screen.dart';
import 'package:get/get.dart';

class Root extends GetWidget<AuthController> {
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Get.find<AuthController>().user != null
          ? ChatScreen()
          : LoginScreen();
    });
  }
}
